const gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyjs = require('gulp-js-minify'),
    imagemin = require('gulp-imagemin'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync').create(),
    babel = require('gulp-babel');

const cleanDist = () => {
    return gulp.src('dist', {read: false})
            .pipe(clean());
}

const scssBuild = () => {
    return gulp.src('src/scss/main.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/css/'));
};

const jsBuild = () => {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(minifyjs())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'));
};

const imagesBuild = () => {
    return gulp.src('src/img/**/*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5
        }))
        .pipe(gulp.dest('dist/img/'));
}

const watch = () => {
    gulp.watch('src/scss/**/*.*', scssBuild).on('change', browserSync.reload);
    gulp.watch('src/js/*.js', jsBuild).on('change', browserSync.reload);
    gulp.watch('*.html', browserSync.reload);
/*
    gulp.watch('index.html', jsBuild).on('change', browserSync.reload);
*/
    /*gulp.watch('*.html', browserSync.reload);*/

    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
};

/*const watch = () => {
    gulp.watch('src/scss/!**!/!*.*', scssBuild).on('change', browserSync.reload);
    gulp.watch('src/js/!*.js', jsBuild).on('change', browserSync.reload);
    gulp.watch('index.html', jsBuild).on('change', browserSync.reload);
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
};*/


gulp.task('dev', watch);
gulp.task('build', gulp.series(cleanDist, scssBuild, jsBuild, imagesBuild));