# test-onlineshop

Использованы технологии:
1.  gulp;
2.  jquery 3.4.1;
3.  bootstrap 4.4.1;
4.  slick 1.6.0;


Студент1 Хистев Тимур:
1.  navbar
2.  header
3.  top menu
4.  top slider
5.  our features
6.  furniture gallery

Студент2 Мельниченко Дмитрий:
1.  stocks
2.  furniture (products)
3.  filters
4.  blog
5.  subscribe to newsletters
6.  copyright with social buttons
