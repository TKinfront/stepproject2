//Smooth scrolling --------------------------------------------
// $(document).ready(function(){
//     let scroll_link = $('.scroll');
//
//     scroll_link.click(function(e){
//         e.preventDefault();
//         let url = $('body').find($(this).attr('href')).offset().top;
//         $('html, body').animate({
//             scrollTop : url
//         },700);
//         $(this).parent().addClass('active');
//         $(this).parent().siblings().removeClass('active');
//         return false;
//     });
// });

$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    focusOnSelect: true
});
